import React, { TextareaHTMLAttributes, useState } from "react";
import "./TextArea.scss";

type TextAreaProps = TextareaHTMLAttributes<HTMLTextAreaElement>

function TextArea({ ...props }: TextAreaProps) {
    return (
        <textarea
            className="textarea"
            {...props}
        />
    );
}

export default TextArea;