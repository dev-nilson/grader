type FeedbackItemProps = {
    sentence: string;
    correction: string;
}

function FeedbackItem({ sentence, correction }: FeedbackItemProps) {
    const isCorrect = sentence.trim() === correction.trim();

    return (
        <div>
            {
                isCorrect
                    ? (<p>Correct!</p>)
                    : (<p>{`${sentence} - ${correction}`}</p>)
            }
        </div>
    )
}

export default FeedbackItem