"use client"
import { getFeedback } from '@/utils/services'
import React, { useState } from 'react'
import Button from '../Button/Button'
import Feedback from '../Feedback/Feedback'
import TextArea from '../TextArea/TextArea'

function Form() {
    const [rows, setRows] = useState(1);
    const [response, setResponse] = useState("");
    const [prompt, setPrompt] = useState("");
    const [loading, setLoading] = useState(false);

    async function handleSubmit(e: React.FormEvent<HTMLFormElement>) {
        e.preventDefault();
        setLoading(true);
        getFeedback(prompt).then(res => {
            setResponse(res.choices[0].text)
            setLoading(false)
        })
    }

    function handleChange(e: React.ChangeEvent<HTMLTextAreaElement>) {
        const newRows = e.target.value.split("\n").length;
        setRows(newRows);
        setPrompt(e.target.value);
    }

    return (
        <>
            <form onSubmit={handleSubmit}>
                <TextArea value={prompt} rows={rows} onChange={handleChange} />
                <Button>Start Grading</Button>
            </form>
            {
                loading
                ? <h1>Loading...</h1>
                : <Feedback request={prompt} response={response} />
            }
        </>
    )
}

export default Form