import FeedbackItem from "../FeedbackItem/FeedbackItem";

type FeedbackProps = {
  request: string
  response: string
}

export default function Feedback({ request, response }: FeedbackProps) {
  const sentences = request.split(/\r?\n/);
  const corrections = response.split("|");

  // getting rid of unwanted string at the beginnin of the response
  corrections[0] = corrections[0].startsWith("\n\n") ? corrections[0].substring(2) : "";

  return (
    <ul>
      {sentences.map((sentence, index) => (
        <li key={index}>
          <FeedbackItem sentence={sentence} correction={corrections[index]} />
        </li>
      ))}
    </ul>
  );
}
