import Link from "next/link"
import "./Navbar.scss";

function Navbar() {
  return (
    <nav className="navbar">
      <div className="navbar__container">
        <Link className="navbar__logo" href="/">
          <h1>Grader</h1>
        </Link>
        <ul className="navbar__menu">
          <Link className="navbar__link" href="/dashboard">Dashboard</Link>
          <Link className="navbar__link" href="/settings">Settings</Link>
        </ul>
      </div>
    </nav>
  )
}

export default Navbar