export async function getFeedback(prompt: string) {
    const result = await fetch("https://api.openai.com/v1/completions", {
        method: "POST",
        headers: {
            "Authorization": `Bearer ${process.env.NEXT_PUBLIC_OPENAI_API_KEY}`,
            "Content-Type": "application/json",
        },
        body: JSON.stringify({
            model: "text-davinci-003",
            prompt: `Correct these sentences and separate them with a | sign: ${prompt}`,
            max_tokens: 200,
        })
    });

    return result.json();
}