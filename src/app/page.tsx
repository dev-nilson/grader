import Workspace from '@/components/Workspace/Workspace'
import { Inter } from '@next/font/google'
import { Suspense } from 'react'
const inter = Inter({ subsets: ['latin'] })

export default function Home() {
  return (
    <main>
      <Workspace />
    </main>
  )
}
